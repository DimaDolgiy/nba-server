# NBA Server

Here's description how to run NBA Server

### Prerequisites

 * [Java SDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html), version 1.8.
 * [Apache Maven](http://maven.apache.org), version 3.3.9 or higher.
 * [Spring Boot](https://projects.spring.io/spring-boot/), version 1.5.4 or higher.
 
### How to run and configurate

* use MYSQL database 
* add an application.properties database conf. (username, password and database name)
* the backend server will start at http://localhost:8080

### Endpoints

* http://localhost:8080/api/nba/players - get all players
* http://localhost:8080/api/nba/teams - get all teams
* http://localhost:8080/api/nba/players/{id} - get list players per team
