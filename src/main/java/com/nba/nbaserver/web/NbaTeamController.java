package com.nba.nbaserver.web;


import com.nba.nbaserver.dto.PlayersDto;
import com.nba.nbaserver.dto.TeamDto;
import com.nba.nbaserver.model.Player;
import com.nba.nbaserver.model.Team;
import com.nba.nbaserver.service.PlayerService;
import com.nba.nbaserver.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/nba")
public class NbaTeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping("/teams")
    public ResponseEntity<List<TeamDto>> getAllTeams() {
        return ResponseEntity.ok(teamService.findAll());
    }





}
