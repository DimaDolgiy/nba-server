package com.nba.nbaserver.web;


import com.nba.nbaserver.dto.PlayersDto;
import com.nba.nbaserver.model.Player;
import com.nba.nbaserver.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/nba")
public class NbaPlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping("/players")
    public List<PlayersDto> getAllPlayers() {
        return playerService.getAllPlayers();
    }

    @PostMapping("/players")
    public ResponseEntity<Player> save(@RequestBody Player player){
        Player p = playerService.save(player);
        return ResponseEntity.status(HttpStatus.CREATED).body(p);
    }

    @GetMapping("/players/{id}")
    public List<PlayersDto> getPlayersPerTeam(@PathVariable("id") long id) {
        return playerService.getPlayersByTeam(id);
    }
}
