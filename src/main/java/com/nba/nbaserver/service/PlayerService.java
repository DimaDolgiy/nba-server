package com.nba.nbaserver.service;

import com.nba.nbaserver.dto.PlayersDto;
import com.nba.nbaserver.model.Player;

import java.util.List;
import java.util.Optional;

public interface PlayerService {

    Player save(Player player);
    List<PlayersDto> getAllPlayers();
    List<PlayersDto> getPlayersByTeam(long id);
    void deleteAll();

}
