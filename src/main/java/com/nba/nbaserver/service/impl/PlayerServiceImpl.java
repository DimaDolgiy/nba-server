package com.nba.nbaserver.service.impl;

import com.nba.nbaserver.dto.PlayersDto;
import com.nba.nbaserver.model.Player;
import com.nba.nbaserver.repository.PlayerRepository;
import com.nba.nbaserver.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public Player save(Player player) {
        if(player != null){
            return playerRepository.save(player);
        }
        return null;
    }


    @Override
    public List<PlayersDto> getAllPlayers() {
        return playerRepository.findAll().stream().map(PlayersDto::new).collect(toList());
    }

    @Override
    public List<PlayersDto> getPlayersByTeam(long id) {
        return playerRepository.findPlayersByTeamId(id).stream().map(PlayersDto::new).collect(toList());
    }

    @Override
    public void deleteAll() {
        playerRepository.deleteAll();
    }
}
