package com.nba.nbaserver.service.impl;


import com.nba.nbaserver.dto.TeamDto;
import com.nba.nbaserver.model.Team;
import com.nba.nbaserver.repository.TeamRepository;
import com.nba.nbaserver.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;


@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Override
    public List<TeamDto> findAll(){
        return teamRepository.findAll().stream().map(TeamDto::new).collect(toList());
    }

    @Override
    public void save(Team team){
        if(team != null){
            teamRepository.save(team);
        }
    }

    @Override
    public void deleteAll() {
        teamRepository.deleteAll();
    }

    @Override
    public Optional<Team> findById(Long id) {
        return teamRepository.findById(id);
    }

}
