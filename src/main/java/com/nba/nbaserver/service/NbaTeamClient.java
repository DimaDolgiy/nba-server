package com.nba.nbaserver.service;

import com.nba.nbaserver.dto.TeamDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class NbaTeamClient {

    @Value("${nba_teams_data_url}")
    private String nbaTeams;

    @Autowired
    private RestTemplate restTemplate;

    public List<TeamDto> getTeamsForeignUrl(){
        ResponseEntity<TeamDto[]> response =
                restTemplate.exchange(nbaTeams, HttpMethod.GET, null, TeamDto[].class);
        return Arrays.asList(response.getBody());
    }

}
