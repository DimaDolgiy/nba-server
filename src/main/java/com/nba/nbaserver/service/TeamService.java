package com.nba.nbaserver.service;

import com.nba.nbaserver.dto.TeamDto;
import com.nba.nbaserver.model.Team;

import java.util.List;
import java.util.Optional;

public interface TeamService {

    List<TeamDto> findAll();
    void save(Team team);
    void deleteAll();
    Optional<Team> findById(Long id);
}
