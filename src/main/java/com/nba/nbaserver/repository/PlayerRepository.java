package com.nba.nbaserver.repository;

import com.nba.nbaserver.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long>{

    @Query("SELECT p from Player p WHERE p.team.id = :id")
    List<Player> findPlayersByTeamId(@Param("id") Long id);

}
