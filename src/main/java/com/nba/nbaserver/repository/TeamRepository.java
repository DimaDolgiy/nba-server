package com.nba.nbaserver.repository;

import com.nba.nbaserver.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface TeamRepository extends JpaRepository<Team, Long>{
}
