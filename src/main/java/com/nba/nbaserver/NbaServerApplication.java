package com.nba.nbaserver;

import com.nba.nbaserver.dto.TeamDto;
import com.nba.nbaserver.model.Player;
import com.nba.nbaserver.model.Team;
import com.nba.nbaserver.service.NbaTeamClient;
import com.nba.nbaserver.service.PlayerService;
import com.nba.nbaserver.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.HashSet;

@SpringBootApplication
public class NbaServerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(NbaServerApplication.class, args);
	}
	@Autowired
	private NbaTeamClient teamClient;
	@Autowired
	private PlayerService playerService;
	@Autowired
	private TeamService teamService;

	@Override
	public void run(String... args) throws Exception {
		initTeams();
		initTestData();

	}

	@Transactional
	void initTeams() {
		teamService.deleteAll();
		teamClient.getTeamsForeignUrl().stream().map(this::transformDtoToEntity).forEach(team -> {
			teamService.save(team);
		});
	}

	@Transactional
	void initTestData(){
		playerService.deleteAll();

		Player player = new Player("James", "Lebron", 321454, 2.17, teamService.findById(1L).get());
		Player player1 = new Player("Dima", "Dolgiy", 321454, 1.76, teamService.findById(1L).get());
		Player player2 = new Player("Michael", "Jordan", 000000, 2.20, teamService.findById(1L).get());
		Player player3 = new Player("Nate", "Robinson", 321454, 1.86, teamService.findById(2L).get());

		Team team = teamService.findById(1L).get();
		Team team1 = teamService.findById(2L).get();

		team.setPlayers(new HashSet<Player>(){{
			add(player);
			add(player1);
			add(player2);
		}
		});

		team1.setPlayers(new HashSet<Player>(){{
			add(player3);
		}
		});

		playerService.save(player);
		playerService.save(player1);
		playerService.save(player2);
		playerService.save(player3);

		teamService.save(team);
		teamService.save(team1);
	}

	private Team transformDtoToEntity(@NotNull TeamDto teamDto){
		return new Team(
				teamDto.getTeamId(),
				teamDto.getFullName(),
				teamDto.getCity(),
				teamDto.getState(),
				teamDto.getDivision(),
				new HashSet<>()
		);
	}
}
