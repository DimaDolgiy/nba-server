package com.nba.nbaserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nba.nbaserver.model.Team;

public class TeamDto {

    private Long id;
    @JsonProperty(value = "team_id", access = JsonProperty.Access.READ_WRITE)
    private String teamId;
    @JsonProperty(value = "full_name", access = JsonProperty.Access.READ_WRITE)
    private String fullName;
    private String city;
    private String state;
    private String division;

    public TeamDto() {
    }

    public TeamDto(Team team) {
        this.id = team.getId();
        this.teamId = team.getTeamId();
        this.fullName = team.getFullName();
        this.city = team.getCity();
        this.state = team.getState();
        this.division = team.getDivision();
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", teamId='" + teamId + '\'' +
                ", fullName='" + fullName + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", division='" + division + '\'' +
                '}';
    }
}
