package com.nba.nbaserver.dto;

import com.nba.nbaserver.model.Player;

public class PlayersDto {

    private Long id;
    private String firstName;
    private String lastName;
    private int phone;
    private double height;
    private TeamDto teamDto;

    public PlayersDto() {
    }

    public PlayersDto(Player player) {
        this.id = player.getId();
        this.firstName = player.getFirstName();
        this.lastName = player.getLastName();
        this.phone = player.getPhone();
        this.height = player.getHeight();
        this.teamDto = new TeamDto(player.getTeam());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public TeamDto getTeamDto() {
        return teamDto;
    }

    public void setTeamDto(TeamDto teamDto) {
        this.teamDto = teamDto;
    }

    @Override
    public String toString() {
        return "PlayersDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone=" + phone +
                ", height=" + height +
                ", teamDto=" + teamDto +
                '}';
    }
}
